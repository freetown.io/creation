#!/bin/bash

# This script creates a governing body for freetown.io. It serves as a charter and constitution
# of the government of freetown.io

# This script will be run once and will initialize the necessary functionality to create, maintain,
# and administer freetown.io through democratic means.


# Citizens form the core of the democratic society of freetown.io. The voting citizenry is made up
# of longtime members of freetown.io and must vote to accept new members into their ranks.

groupadd citizens

# Every user starts as a provisional user. These users are not allowed to vote but may still
# provisionally take part in freetown.io society. Exceptional provisional citizens may be
# nominated to become full citizens.

groupadd prov


# Exectutors have absolute control over the system. They may run any command at any time. 
# Users may not become an executor through any normal process, and these users should only
# use their power when absolutely necessary.

# This power is deemed useful for fixing bugs in the created system as bugs may render the
# voting citizenry unable to carry out any action. This power may be used for the defense of
# freetown.io, to defend the public and restore order after a breach.

groupadd executors

# And allow executors to execute any command using sudo
echo "%executors ALL=(ALL:ALL) ALL" >> /etc/sudoers



# The freetown user owns everything related to the town. This user will run all the automatic
# infrastructure that makes freetown run.

useradd freetown
echo "freetown ALL=(ALL:ALL) ALL" >> /etc/sudoers


# Install the utilities needed for running freetown
apt update
apt install -y acl gawk gpg

# The town hall is created as the public forum of freetown.io. This is created as follows:

# /townhall is the root of the town hall. Citizens are allowed to create files and directories
# in this directory, letting citizens actively take part in creating new ways to interact with
# the town. This also makes it easy and encourages citizens to share projects and creative works.

mkdir -p /townhall
chown freetown:freetown /townhall
chmod 775 /townhall
setfacl -m "g:citizens:rwx" /townhall


# /townhall/creation contains the documents used to set up freetown. These are meant mainly
# for documentation and should not be modified.

mkdir -p /townhall/creation
chmod 775 /townhall/creation


# /townhall/acts contains a read-only copy of all scripts that have been voted on and ran. 
# Henceforth these scripts will be referred to as acts. Acts can create self-enforcing laws,
# relying on the UNIX environment for enforcement. Acts may also execute any privileged command
# and can be used to create user-enforceable laws that are too complex to be enforced by UNIX.
# Acts may be used to add new users to the citizenry of freetown and conversely may be used
# to exile users from the citizenry or even the town.

mkdir -p /townhall/acts
chmod 775 /townhall/acts


# /townhall/proposed contains scripts that are going to be up for voting at some point
# in the future. Any user can add scripts into this directory and when they are ready
# they can trigger votectl to move the law into /townhall/active, or a user can manually
# activate the same process with a simple `mv`

mkdir -p /townhall/proposed
chmod 775 /townhall/proposed
setfacl -m "g:citizens:rwx" /townhall/proposed


# /townhall/unratified holds all the scripts that have been up for a vote and failed to 
# gain the necessary votes to be run.

mkdir -p /townhall/unratified
chmod 775 /townhall/unratified


# /townhall/active holds all the acts that are up for a vote. Users can read the contents
# of these acts and discuss and vote on them.

mkdir -p /townhall/active
chmod 775 /townhall/active

# /townhall/staging is a staging ground for bills. The directory gets scanned every few seconds
# and if any new files are found they are reowned by the town and moved to /townhall/active.
# This is how users put new bills up for a vote.

mkdir -p /townhall/staging
chmod 775 /townhall/staging
setfacl -m "g:citizens:rwx" /townhall/staging


# /townhall/laws should contain plain english or simply formatted laws. These are usually 
# not enforceable by the operating system and are designed to be read and understood by
# the public. Nothing is automatically created here, but acts can create files in this
# directory. These laws are enforceable through public vote.

mkdir -p /townhall/laws
chmod 775 /townhall/laws


# /townhall/keys holds the public pgp keys of every citizen. These keys are used when 
# verifying the gpg signatures on votes.
mkdir -p /townhall/keys
chmod 775 /townhall/keys

# Reown all directories in the townhall to the town.
chown -R freetown:freetown /townhall

# Set the timezone to UTC. There might be people connecting to freetown from all over
# so UTC seems like a neutral timezone to pick.
timedatectl set-timezone UTC

# Copy the magistratus to the town hall and create a cron job to continuously run it
cp /townhall/creation/magistratus /townhall/magistratus

# Check every 2 minutes for new bills and whether bills should be immediately passed
echo "*/2 * * * * root /townhall/magistratus ratification_check" >> /etc/crontab
echo "*/2 * * * * root /townhall/magistratus create_bills" >> /etc/crontab

# Finish the session on Sunday at 2AM (PST)
echo "* 9 * * 0 root /townhall/magistratus finalize" >> /etc/crontab
